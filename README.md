On the Lambda
=============

Minimalistic zsh theme

## Install

Move `on-the-lambda.zsh-theme` to an appropriate directory. Then add the
following line in your `~/.zshrc`:

```shell
source /PATH/TO/DIR/on-the-lambda.zsh-theme
```
