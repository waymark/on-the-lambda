# vim:filetype=zsh
#
# On the Lambda
# -- --- ------
# a git-aware zsh theme
#
# Inspired by https://github.com/ergenekonyigit/lambda-gitster

function get_wd() {
    local wd="%B%F{blue}$(print -D $PWD)%f%b"
    echo $wd | sed -E "s#/#%F{cyan}/%F{blue}#g"
}

# https://github.com/robbyrussell/oh-my-zsh/blob/master/lib/git.zsh#L132
function git_status() {
    local display="%F{black}%K{green}"
    local conflict_fmt="%F{black}%K{red}"
    local modified_fmt="%F{black}%K{yellow}"
    local staged_fmt="%F{black}%K{cyan}"

    if $(git rev-parse MERGE_HEAD &> /dev/null); then
        local is_dirty=1
        display=$conflict_fmt
    else
        local status_=$(git status --porcelain -unormal 2> /dev/null)

        if $(echo $status_ | grep -E '^.[MARCD]' &> /dev/null); then
            local is_dirty=1
            display=$modified_fmt
        elif $(echo $status_ | grep -E '^\?\?' &> /dev/null); then
            display=$modified_fmt
        fi

        if $(echo $status_ | grep -E '^[MARCD]' &> /dev/null); then
            display=$staged_fmt
        fi
    fi

    if [[ $is_dirty ]]; then
        echo "$display ×"
    else
        echo "$display"
    fi
}

function git_formatted() {
    local branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)
    if [[ -z $branch ]]; then
        return
    fi

    if [[ $branch == 'HEAD' ]]; then
        branch=$(git rev-parse --short HEAD 2> /dev/null)
    fi

    local num_stashed=$(git stash list | wc | awk '{ print $1 }' 2> /dev/null)
    local stash=$([[ $num_stashed != '0' ]] && echo "[$num_stashed]")

    echo "$(git_status) $branch$stash %k%f "
}

function theme_precmd() {
    local END="%(? %F{cyan} %F{red})λ"
    PROMPT="$(get_wd) $(git_formatted)$END %f%k"
}

function setup() {
    autoload -Uz add-zsh-hook

    add-zsh-hook precmd theme_precmd
}

setup
